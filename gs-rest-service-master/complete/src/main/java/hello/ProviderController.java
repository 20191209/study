package com.taikang.provider.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.taikang.provider.service.ProviderService;

@RestController
public class ProviderController {
	
	@Autowired
	ProviderService providerService;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void test() {
		System.out.println("=======provider===========");
		providerService.test();
	}

	public static void main(String[] args) {
//        String[] m = {"A","B","C","D","E"};
//        int[] n = {3,43,23,12,32,100};
//        Set<Set<String>> combinationAll = combination(m);
//        int[] test = test(n);
//        for (int i : test) {
//			System.err.println("=============" + i);
//		}
//        int test = test(n);
//        System.err.println(test);
		test1();
    }

    private static  Set<Set<String>> combination(String[] m) {
        Set<Set<String>> result = new HashSet<>();

        for (int i = 1; i < Math.pow(2,m.length) - 1; i++) {

            Set<String> eligibleCollections = new HashSet<>();
            for (int j = 0; j < m.length; j++) {
                if ((i & (int)Math.pow(2,j)) == Math.pow(2,j)){
                    eligibleCollections.add(m[j]);
                }
            }

            result.add(eligibleCollections);
        }

        return  result;
    }
    
	private static int test(int[] m) {
		int g = 0;
		try {
			int n;
			for (int i = 0; i < m.length; i++) {
				for (int j = 0; j < m.length - i -1; j++) {
					if(m[j] > m[j+1]) {
						n = m[j+1];
						m[j+1] = m[j];
						m[j] = n;
					}
				}
			}
			char b = 'a' + 1;
			System.err.println("==s==" + b);
			g++;
			return g;
		} catch (Exception e) {
			e.printStackTrace();
			
		}finally {
			++g;
			//return g;
			System.out.println("finaly");
		}
		System.out.println("finaly---------out");
		return 3;
		
	}
	
	public static void test1() {
		Integer reduce = Stream.of(1,2,3,4).reduce(10, (count, time) -> {
			System.err.println(count);
			System.err.println(time);
			return count + time;
			});
		Integer reduce2 = Stream.of(1,2,3,4).reduce((a, b) -> {return a + b;}).get();
		System.err.println(reduce2);
	}
	
	public double test2(List<Integer> list) {
		return list.stream().map(r -> r * 1).reduce((a, b) -> a + b).map(Integer :: doubleValue).orElse(0d);
	}
	
}
